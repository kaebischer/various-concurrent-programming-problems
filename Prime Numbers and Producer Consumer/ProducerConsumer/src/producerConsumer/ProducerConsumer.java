package producerConsumer;

import java.util.Arrays;

public class ProducerConsumer{
	static Object lock = new Object();
	volatile static int in = 0;
	volatile static int out = 0;
	volatile static int bufferCurrentSize = 0;
	static int[] circularBuffer;
	static int timeOut;
	static class ProducerThread implements Runnable {
		int id;
		int n;
		
		public ProducerThread(int id, int n) {
			this.id = id;
			this.n = n;
		}
		
		@Override
		public void run() {
			if(timeOut == 0) {
				while (true) {
					synchronized(lock) {
						while(bufferCurrentSize == (n-1)) {
							try {
								lock.wait();
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
						}
						circularBuffer[in] = 1;
						bufferCurrentSize++;
						System.out.printf("Producer n° %d has produced at position: %d\n", id, in);
						in = (in + 1) % n;
						System.out.printf("Current state of circular buffer: %s\n", Arrays.toString(circularBuffer));
						lock.notifyAll();
						
					}
				}
			}else {
				while (timeOut > 0) {
					synchronized(lock) {
						while(bufferCurrentSize == (n-1)) {
							try {
								lock.wait();
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
						}
						circularBuffer[in] = 1;
						bufferCurrentSize++;
						System.out.printf("Producer n° %d has produced at position: %d\n", id, in);
						in = (in + 1) % n;
						System.out.printf("Current state of circular buffer: %s\n", Arrays.toString(circularBuffer));
						timeOut--;
						lock.notifyAll();
						
					}
				}
			}
			
		}
	}
	
	static class ConsumerThread implements Runnable {
		int id;
		int n;
		
		public ConsumerThread(int id, int n) {
			this.id = id;
			this.n = n;
		}
		
		@Override
		public void run() {
			if(timeOut == 0) {
				while (true) {
					synchronized(lock) {
						while(in == out && bufferCurrentSize <= 0) {
							try {
								lock.wait();
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
						}
						circularBuffer[out] = 0;
						bufferCurrentSize--;
						System.out.printf("Consumer n° %d has consumed at position: %d\n", id, out);
						out = (out + 1) % n;
						System.out.printf("Current state of circular buffer: %s\n", Arrays.toString(circularBuffer));
						lock.notifyAll();
					}
				}
			}else {
				while (timeOut > 0) {
					synchronized(lock) {
						while(in == out && bufferCurrentSize <= 0) {
							try {
								lock.wait();
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
						}
						circularBuffer[out] = 0;
						bufferCurrentSize--;
						System.out.printf("Consumer n° %d has consumed at position: %d\n", id, out);
						out = (out + 1) % n;
						System.out.printf("Current state of circular buffer: %s\n", Arrays.toString(circularBuffer));
						timeOut--;
						lock.notifyAll();
					}
				}
			}
			
		}
	}
	
	public static void main(String[] args) {
		int t = (args.length >= 1 ? Integer.parseInt(args[0]) : 4);
		int n = (args.length >= 2 ? Integer.parseInt(args[1]) : 16);
		timeOut = (args.length >= 3 ? Integer.parseInt(args[1]) : 50);
		
		circularBuffer = new int[n];
		for(int i = 0; i < circularBuffer.length; i++) {
			circularBuffer[i] = 0;
		}
		
		System.out.printf("Start with %d threads\n", t);
		System.out.printf("Counter's range is [1,%d]\n", n);
		// Create threads
		Thread[] threads = new Thread[t*2];
		for (int i = 0; i < t; i++) {
			threads[i] = new Thread(new ProducerThread(i+1, n));
		}
		for (int i = t; i < t*2; i++) {
			threads[i] = new Thread(new ConsumerThread(i, n));
		}
		long time = System.currentTimeMillis();
		// Start threads
		for (int i = 0; i < t*2; i++) {
			threads[i].start();
		}
		// Wait for threads completion
		for (int i = 0; i < t*2; i++) {
			try {
				threads[i].join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		time = System.currentTimeMillis() - time;
		System.out.printf("\nTime out reached. End state of circular buffer:%s  \n%d ms", Arrays.toString(circularBuffer), time);
	}
}