package primeNumbers;

import java.util.ArrayList;


public class PrimeNumbersCounter {
	
	static long counter = 1;
	static Object lock = new Object();
	static ArrayList<Long> primes = new ArrayList<Long>();
	
	static class PrimeNumbersCounterThread implements Runnable {
		
		int id;
		int n;
		long maxRange;
		long number;
		
		public PrimeNumbersCounterThread(int id, int n, long maxRange) {
			this.id = id;
			this.n = n;
			this.maxRange = maxRange;
		}
		
		@Override
		public void run(){
			Boolean isPrime = true;
			
			while(counter <= maxRange) {
				synchronized(lock) {
					number = counter;
					counter++;
				}
				
				for(int i = 2; i<=number/2; i++) {
					if(number%i == 0) {
						isPrime = false;
						break;
					}else {
						isPrime = true;
					}	
				}
				if(isPrime && number > 1) {
					synchronized(lock) {primes.add(number);}
				}else {
					continue;
				}
				
			}
		}
	}
	
	public static void main(String[] args) {
		int t = (args.length >= 1 ? Integer.parseInt(args[1]) : 4); //number of threads
		int n = (args.length >= 2 ? Integer.parseInt(args[1]) : 4); //number of cores
		long maxRange = (long) (Math.pow(n, 10));
		
		System.out.printf("Start with %d threads\n", t);
		System.out.printf("Counter's range is [1,%d]\n", maxRange);
		// Create threads
		Thread[] threads = new Thread[t];
		for (int i = 0; i < t; i++) {
			threads[i] = new Thread(new PrimeNumbersCounterThread(i+1, n, maxRange));
		}
		long time = System.currentTimeMillis();
		// Start threads
		for (int i = 0; i < t; i++) {
			threads[i].start();
		}
		// Wait for threads completion
		for (int i = 0; i < t; i++) {
			try {
				threads[i].join();
			} catch (InterruptedException e) {
			}
		}
		time = System.currentTimeMillis() - time;
		System.out.printf("\nList of prime numbers: %s \nDuration: %d ms", primes ,time);
	}
}