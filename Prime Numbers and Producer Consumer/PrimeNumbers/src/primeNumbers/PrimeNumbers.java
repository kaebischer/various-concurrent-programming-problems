package primeNumbers;

import java.lang.Math;
import java.util.ArrayList;


public class PrimeNumbers {
	
	static Object lock = new Object();
	
	static ArrayList<Long> primes = new ArrayList<Long>();
	
	static class PrimeNumbersThread implements Runnable {
		
		int id;
		int n;
		int t;
		long minRange;
		long maxRange;
		//long number;
		
		public PrimeNumbersThread(int id, int n, int t) {
			this.id = id;
			this.n = n;
			this.t = t;
		}
		
		@Override
		public void run() {
			if(t == 1) {
				minRange = (long) 1;
				maxRange = (long) (Math.pow(n, 10));
			}else {
				minRange = (long) ((id-1) * Math.pow(n, 9) + 1);
				maxRange = (long) (id * Math.pow(n, 9));
			}
			
			Boolean isPrime = true;
			
			System.out.printf("Thread n° %d has the range [%d,%d]\n", id, minRange, maxRange);
			
			for(long number = minRange; number <= maxRange; number++) {
				for(int i = 2; i<=number/2; i++) {
					if(number%i == 0) {
						isPrime = false;
						break;
					}else {
						isPrime = true;
					}	
				}
				if(isPrime && number > 1) {
					synchronized(lock) {primes.add(number);}
				}else {
					continue;
				}
			}
		}
	}
	
	public static void main(String[] args) {
		int t = (args.length >= 1 ? Integer.parseInt(args[0]) : 4); //number of threads
		int n = (args.length >= 2 ? Integer.parseInt(args[0]) : 4); //number of cores

		System.out.printf("Start with %d threads\n", t);
		// Create threads
		Thread[] threads = new Thread[t];
		for (int i = 0; i < t; i++) {
			threads[i] = new Thread(new PrimeNumbersThread(i+1, n, t));
		}
		long time = System.currentTimeMillis();
		// Start threads
		for (int i = 0; i < t; i++) {
			threads[i].start();
		}
		// Wait for threads completion
		for (int i = 0; i < t; i++) {
			try {
				threads[i].join();
			} catch (InterruptedException e) {
			}
		}
		time = System.currentTimeMillis() - time;
		System.out.printf("\nList of prime numbers: %s \nDuration: %d ms", primes ,time);
	}
}