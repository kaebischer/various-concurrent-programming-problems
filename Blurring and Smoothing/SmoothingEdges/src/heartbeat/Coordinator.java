package heartbeat;


public class Coordinator implements Runnable{
	int id;
	Communication communication;
	
	public Coordinator(int id, Communication communication) {
		this.id = id;
		this.communication = communication;
	}

	@Override
	public void run() {
		boolean change = true;
		
		while (change) {
			change = false;		
			change = communication.receiveResult(change);
			communication.sendAnswer(change);
		}
	}
}
