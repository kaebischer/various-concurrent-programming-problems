package heartbeat;

import java.util.Arrays;

public class Worker implements Runnable{
	int id;
	int nbWorker;
	int nbBlurring;
	int[][] image;
	int stripSize;
	Communication communication;
	volatile int[][] imageStrip;
	
	public Worker(int id, int nbWorker, int nbBlurring, int[][] image, Communication communication) {
		this.id = id;
		this.nbWorker = nbWorker;
		this.nbBlurring = nbBlurring;
		this.image = image;
		this.communication = communication;
	}
	
	@Override
	public void run() {
		stripSize = image.length / nbWorker; //local values plus edges
		boolean change = true;
		
		// Initialise imageStrip with extra line of pixel for neighbours
		imageStrip = new int[stripSize + 2][image[0].length];
		
		initStrip(imageStrip, stripSize);
		
		while (change) {
			
			exchangeEdges();
			
			change = applyBlurring(imageStrip);

			communication.sendResult(id, change);
			
			change = communication.receiveAnswer();
		}
		writeBack(imageStrip, stripSize);
	}
	
	/*
	 * Exchange edges with neighbours
	 * */
	public void exchangeEdges() {
		if (id != 1) {
			communication.sendFirstLine(id-1, imageStrip[1]);//to worker above
		}
		
		if (id != nbWorker) {
			communication.sendLastLine(id+1, imageStrip[stripSize]);//to worker below
		}
		
		if (id != nbWorker) {
			updateLastLine(communication.receiveLastLine(id), imageStrip);//from worker below
		}
		
		if (id != 1) {
			updateFirstLine(communication.receiveFirstLine(id), imageStrip);//from worker above
		}
	}
	
	/*
	 * Update first line of the local image strip
	 * */
	public void updateFirstLine(int[] edge, int[][] imageStrip) {
		for (int j = 0; j < edge.length; j++) {
			imageStrip[0][j] = edge[j];
		}
	}
	
	/*
	 * Update last line of the local image strip
	 * */
	public void updateLastLine(int[] edge, int[][] imageStrip) {
		for (int j = 0; j < edge.length; j++) {
			imageStrip[imageStrip.length-1][j] = edge[j];
		}
	}
	
	/*
	 * Write back result of the local image strip to the initial shared image
	 * */
	public synchronized void writeBack(int[][] imageStrip, int stripSize) {
		int startIndex = stripSize * (id-1); // compute relative starting index of the current worker in the shared image
		int lastIndex = startIndex + stripSize;
		
		for (int i = startIndex; i < lastIndex; i++) {
			for (int j = 0; j < imageStrip[0].length; j++) {
				image[i][j] = imageStrip[i+1-startIndex][j];
			}
		}		
	}
	
	/*
	 * Initialise local image strip
	 * */
	public void initStrip(int[][] imageStrip, int stripSize) {
		// first fill all lines with -1
		for (int[] arr : imageStrip) {
        	Arrays.fill(arr, -1);
        }

		for (int i = stripSize*(id-1); i < (stripSize*id); i++) {
			for (int j = 0; j < imageStrip[0].length; j++) {
				imageStrip[i%stripSize+1][j] = image[i][j];
			}
		}
	}
	
	/*
	 * Compute the blurring algorithm (average blur technique)
	 * 
	 * @return change
	 * */
	public boolean applyBlurring(int[][] imageStrip) {
		boolean change = true;
		int startIndex = 1;
		int lastIndex = imageStrip.length - 1;
		
		for (int i = startIndex; i < lastIndex; i++) {
			for (int j = 0; j < imageStrip[0].length; j++) {
				// first line -> no neighbours above
				if (i == startIndex && id == 1) {
					// first column -> no left neighbour (left corner)
					// 3 neighbours
					if (j == 0) {
						imageStrip[i][j] = (imageStrip[i][j+1] + imageStrip[i+1][j+1] + 
											imageStrip[i+1][j] + imageStrip[i][j])/4;
					// last column -> no right neighbour (right corner)
					// 3 neighbours
					} else if (j == imageStrip[0].length - 1) {
						imageStrip[i][j] = (imageStrip[i][j-1] + imageStrip[i+1][j-1] + 
											imageStrip[i+1][j] + imageStrip[i][j])/4;
					// in between in the first line
					// 5 neighbours
					} else {
						imageStrip[i][j] = (imageStrip[i][j-1] + imageStrip[i+1][j-1] + 
											imageStrip[i+1][j] + imageStrip[i+1][j+1] + 
											imageStrip[i][j+1] + imageStrip[i][j])/6;
					}
				// last line -> no neighbours below
				} else if (i == lastIndex-1 && id == nbWorker) {
					// first column -> no left neighbour (left corner)
					// 3 neighbours
					if (j == 0) {
						imageStrip[i][j] = (imageStrip[i][j+1] + imageStrip[i-1][j+1] + 
											imageStrip[i-1][j] + imageStrip[i][j])/4;
					// last column -> no right neighbour (right corner)
					// 3 neighbours
					} else if (j == imageStrip[0].length - 1) {
						imageStrip[i][j] = (imageStrip[i-1][j] + imageStrip[i-1][j-1] + 
											imageStrip[i][j-1] + imageStrip[i][j])/4;
					// in between in the last line
					// 5 neighbours
					} else {
						imageStrip[i][j] = (imageStrip[i][j-1] + imageStrip[i-1][j-1] + 
											imageStrip[i-1][j] + imageStrip[i-1][j+1] + 
											imageStrip[i][j+1] + imageStrip[i][j])/6;
					}
				// in between 
				} else {
					// first column -> no left neighbour
					// 5 neighbours
					if (j == 0) {
						imageStrip[i][j] = (imageStrip[i-1][j] + imageStrip[i-1][j+1] + 
											imageStrip[i][j+1] + imageStrip[i+1][j+1] + 
											imageStrip[i+1][j] + imageStrip[i][j])/6;
					// last column -> no right neighbour
					// 5 neighbours
					} else if (j == imageStrip[0].length - 1 ) {
						imageStrip[i][j] = (imageStrip[i][j-1] + imageStrip[i-1][j-1] + 
											imageStrip[i-1][j] + imageStrip[i+1][j] + 
											imageStrip[i+1][j-1] + imageStrip[i][j])/6;
					// in between
					// 8 neighbours
					} else {
						imageStrip[i][j] = (imageStrip[i][j-1] + imageStrip[i-1][j-1] + 
											imageStrip[i-1][j] + imageStrip[i-1][j+1] + 
											imageStrip[i][j+1] + imageStrip[i+1][j+1] + 
											imageStrip[i+1][j] + imageStrip[i+1][j-1] + 
											imageStrip[i][j])/9;
					}
				}
			}
		}
		if (--nbBlurring == 0) {
			change = false;
		}
		return change;
	}
}
