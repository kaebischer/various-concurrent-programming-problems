package heartbeat;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;


public class Main {
	static int[] pgmParam = new int[4];
	volatile static int[][] image;
	
	@SuppressWarnings("unused")
	public Main() {
		// Initialise array of pgm's parameters (magic, height, width, max)
		for (int elem : pgmParam) {
			elem = 0;
		}
	}
	
	public static void main(String[] args) throws NumberFormatException, IOException {
		int t = (args.length >= 1 ? Integer.parseInt(args[0]) : 8); // number of threads (worker)
		int d = (args.length >= 2 ? Integer.parseInt(args[1]) : 1); // number of blurring computations
		String pgmName = (args.length >= 3 ? args[2] : "balloons"); // name of the pgm file
		
		
		System.out.printf("Start with %d worker threads\n", t);
		System.out.printf("Number of blurring application is %d\n\n", d);
		
		Communication communication = new Communication(t);
		
		// Get the image and prepare the two dimensional array
		image = pullPgm(pgmName);
		
		// Create threads
		Thread[] threads = new Thread[t+1]; // t workers + 1 coordinator
		for (int i = 0; i <= t; i++) {
			if(i == 0) {
				threads[i] = new Thread(new Coordinator(i, communication));
			} else {
				threads[i] = new Thread(new Worker(i, t, d, image, communication));
			}
			
		}

		long time = System.currentTimeMillis();
		
		// Start threads
		for (int i = 0; i <= t; i++) {
			threads[i].start();
		}
		
		// Wait for threads completion
		for (int i = 0; i <= t; i++) {
			try {
				threads[i].join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		time = System.currentTimeMillis() - time;
		
		pushPgm(pgmName + "blurred" +".pgm", "P" + pgmParam[0], pgmParam[1], pgmParam[2], pgmParam[3], image);
		
		System.out.printf("\nCompleted in %d ms", time);
	}
	
	/*
	 * Write the resulted pgm file after applying the smoothing algorithm
	 * */
	public static void pushPgm(String fileName, String magic, int height, int width, int max, int[][]data) throws IOException{
	    DataOutputStream writeFile = new DataOutputStream(new FileOutputStream(fileName)); 
	     // Write the .pgm header (P2, hight width, max) 
	    writeFile.writeBytes(magic + "\n");
	    writeFile.writeBytes(width + " " + height + "\n");
	    writeFile.writeBytes(max + "\n");

	    for(int i = 0; i < height; i++){
	        for(int j = 0; j < width; j++){
	        	// Write the pixel number
	            writeFile.writeBytes(data[i][j]+" "); 
	        }
	        // Write new line at each ending line
	        writeFile.writeBytes("\n"); 
	    }
	    writeFile.close();
	}
	
	/*
	 * Retrieve values of the initial pgm file and put them into two dimensional array
	 * 
	 * @return initial image into matrix
	 * */
	public static int[][] pullPgm(String pgmName) {
		String dir = System.getProperty("user.dir");
		String imagePath = "/images/"+pgmName+".pgm";
		Scanner scan;
		int[][] image = null;
		
		FileInputStream fileStream;
	    try {
	    	fileStream = new FileInputStream(dir+imagePath);
	    	BufferedReader br = new BufferedReader(new InputStreamReader(fileStream));
	    	String magic = br.readLine();    // first line contains P2 or P5
	    	
	    	if (!"P2".equals(magic)) {
            	System.out.println("ERROR not suitable PGM file :::: java programm has stopped!!!");
                System.exit(1);
            }
	    	
	    	String line;
	    	if ((line = br.readLine()).startsWith("#")) { // check if there are any pgm comments
	    		while ((line = br.readLine()).startsWith("#")) {
	    			continue;
	    		}
	    	}
	    	
	        // scan width and height
	    	scan = new Scanner(line);
	        int width = scan.nextInt();
	        int height = scan.nextInt();
	        
	        // close scanner
	        scan.close();
	        
	        // scan max value
	        line = br.readLine();
	        scan = new Scanner(line);
	        int maxVal = scan.nextInt();
	        
	        // close scanner
	        scan.close();

	        image = new int[height][width];

            int count = 0;
            
            while (line != null) {

            	while((line = br.readLine()) != null) {
            		final String[] tokens = line.trim().split(" +"); // handle multiple space between pixel value

            		for (String arr : tokens) {
            			image[count / width][count % width] = (char) Integer.parseInt(arr);
            			count++;
                    }
            	}
            }
            
	        // close buffer
	        br.close();
	        fileStream.close();
	        
	        pgmParam[0] = Integer.parseInt(magic.substring(1));
	        pgmParam[1] = height;
	        pgmParam[2] = width;
	        pgmParam[3] = maxVal;

	    } catch (FileNotFoundException e) {
	        e.printStackTrace();
	    } catch (IOException e) {
	        e.printStackTrace();
	    }
	    
		return image;
	}
}
