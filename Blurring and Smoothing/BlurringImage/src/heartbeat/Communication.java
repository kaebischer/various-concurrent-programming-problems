package heartbeat;


import java.util.LinkedList;
import java.util.Queue;


public class Communication {
	Object waitingRoom = new Object();
	int nbWorkers;
	volatile Queue<int[]>[] mailBoxFirstLine;
	volatile Queue<int[]>[] mailBoxLastLine;
	boolean[] mailBoxResult;
	boolean coordinatorAnswer;
	volatile int counter = 0;
	
	@SuppressWarnings("unchecked")
	public Communication (int nbWorkers) {
		this.nbWorkers = nbWorkers;
		this.mailBoxFirstLine = new Queue[nbWorkers];

		for (int i = 0; i < nbWorkers; i++) {
			this.mailBoxFirstLine[i] = new LinkedList<int[]>();
		}
			
		this.mailBoxLastLine = new Queue[nbWorkers];
		for (int i = 0; i < nbWorkers; i++) {
			this.mailBoxLastLine[i] = new LinkedList<int[]>();
		}
		
		this.mailBoxResult = new boolean[nbWorkers];
		this.coordinatorAnswer = true;
	}
	
	/*
	 * Put first line in the mail box of my neighbour
	 * */
	public void sendFirstLine(int neighbour, int[] edge) {
		mailBoxFirstLine[neighbour-1].add(edge);
	}
	
	/*
	 * Put last line in the mail box of my neighbour
	 * */
	public void sendLastLine(int neighbour, int[] edge) {
		mailBoxLastLine[neighbour-1].add(edge);
	}
	
	/*
	 * Get last line in my mail box
	 * 
	 * @return last line
	 * */
	public int[] receiveLastLine(int id) {
		while (mailBoxFirstLine[id-1].isEmpty()) {
			// busy wait
		}
	    return mailBoxFirstLine[id-1].remove(); 
	}
	
	/*
	 * Get first line in my mail box
	 * 
	 * @return first line
	 * */
	public int[] receiveFirstLine(int id) {
		while (mailBoxLastLine[id-1].isEmpty()) {
			// busy wait
		}
	    return mailBoxLastLine[id-1].remove(); 
	}
	
	/*
	 * Workers put change result in result mail box for coordinator and wait
	 * */
	public void sendResult(int id, boolean change) {
		mailBoxResult[id-1] = change;
		
		synchronized(waitingRoom) {   
			try {
				counter++;
				waitingRoom.wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
	}
	
	/*
	 * Workers get change answer from coordinator
	 * 
	 * @return coordinator answer
	 * */
	public boolean receiveAnswer() {
		return coordinatorAnswer;
	}
	
	/*
	 * Coordinator put change answer for worker and notify them
	 * */
	public void sendAnswer(boolean change) {
		coordinatorAnswer = change;
		synchronized(waitingRoom) {   
			waitingRoom.notifyAll();
		}
	}
	
	/*
	 * Coordinator gather change result from all workers
	 * 
	 * @return change result
	 * */
	public boolean receiveResult(boolean change) {
		// wait until all worker send result
		while (counter != nbWorkers) {
			// busy wait
		}
		
		for (int i = 0; i < mailBoxResult.length; i++) {
			change = mailBoxResult[i] || change; 
		}

		counter = 0;
		return change;
	}
}
