package peterson;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicIntegerArray;

public class Peterson {
	static volatile AtomicInteger sharedCounter;
	static volatile AtomicIntegerArray counterNbAccesses;
	static volatile AtomicIntegerArray level;
	static volatile AtomicIntegerArray victim;

	public static void lock(int threadId) {
		for(int l = 1; l < level.length(); l++) {
			level.set(threadId, l);
			victim.set(l,threadId);
			for(int k = 0; k < level.length(); k++) {
				while(k != threadId && level.get(k) >= l && victim.get(l) == threadId) {
					//busy wait
				}
			}
		}
	}
	
	public static void unlock(int threadId) {
		level.set(threadId, 0);
	}
	
	public static void printMinMax(AtomicIntegerArray counterNbAccesses) {
		int min = counterNbAccesses.get(0), max = counterNbAccesses.get(0), indexMin=0, indexMax = 0;
		for(int i = 1; i < counterNbAccesses.length(); i++)
	    {
	        if(max < counterNbAccesses.get(i))
	        {
	            max = counterNbAccesses.get(i);
	            indexMax = i;
	        }

	        if(min > counterNbAccesses.get(i))
	        {
	            min = counterNbAccesses.get(i);
	            indexMin = i;
	        }
	    }
		System.out.printf("\n- Lowest accesses number for a thread: T_%d, %d", indexMin, min);
		System.out.printf("\n- Highest accesses number for a thread: T_%d, %d", indexMax, max);
	}
	
	public static void printResult(long timeExecution) {
		System.out.printf("\n- Shared counter limit reached with state: %s \n- End state of counter accesses:%s ", sharedCounter.toString(), counterNbAccesses.toString());
		printMinMax(counterNbAccesses);
		System.out.printf("\n\n%d ms", timeExecution);
		System.out.println("\n------------------------------------------------------------------------------------------");
	}
	
	public static class ThreadIncrement implements Runnable {
		int id;
		int limit;
		
		public ThreadIncrement(int id, int limit) {
			this.id = id;
			this.limit = limit;
		}
		
		@Override
		public void run() {
			while(Peterson.sharedCounter.get() <= limit - Peterson.level.length()) {
				Peterson.lock(id);
				Peterson.sharedCounter.getAndIncrement();
				Peterson.counterNbAccesses.getAndIncrement(id);
				Peterson.unlock(id);
			}
		}
	}
	
	public static void main(String[] args) {

		int t = (args.length >= 1 ? Integer.parseInt(args[0]) : 4);
		int runNumber = (args.length >= 2 ? Integer.parseInt(args[1]) : 5);
		int sharedCounterLimit = 3000000;
		
		System.out.printf("Peterson Volatile version is going to run %d times", runNumber);
		System.out.printf("\nStart with %d threads\n", t);
		System.out.printf("Shared counter limit is %d\n", sharedCounterLimit);
		
		for (int j=0; j<runNumber; j++) {
			counterNbAccesses = new AtomicIntegerArray(t);
			level = new AtomicIntegerArray(t);
			victim = new AtomicIntegerArray(t);
			sharedCounter = new AtomicInteger();
			
			// Create threads
			Thread[] threads = new Thread[t];
			for (int i = 0; i < t; i++) {
				threads[i] = new Thread(new ThreadIncrement(i, sharedCounterLimit));
			}
		
			long time = System.currentTimeMillis();
			
			// Start threads
			for (int i = 0; i < t; i++) {
				threads[i].start();
			}
			
			// Wait for threads completion
			for (int i = 0; i < t; i++) { 
				try {
					threads[i].join();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			
			time = System.currentTimeMillis() - time;
			
			printResult(time);
		}
	}
}