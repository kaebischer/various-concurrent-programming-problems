package diningSavages;

public class Savage implements Runnable {
	int id;
	Pot pot;
	int limit;
	int numberSavages;


	public Savage(int id, Pot pot, int numberSavages) {
		this.id = id;
		this.pot = pot;
		this.limit = pot.numberMealsRestriction;
		this.numberSavages = numberSavages;
	}

	@Override
	public void run() {
		while (true) {
			if (limit == 0) {
				System.out.printf("\n Savage %d: Limit reached", id);
				while(pot.fairnessFlag == false) {
					//wait for fairness
				}
				while (pot.fairnessFlag == true) {
					if (pot.accessToPot.tryLock()) {
						pot.fairnessLimit++;
						if (pot.fairnessLimit == numberSavages * pot.numberMealsRestriction)
							pot.fairnessFlag = false;
						pot.accessToPot.unlock();
					}
				}
				System.out.printf("\n Savage %d: Leave fairness", id);
				limit = pot.numberMealsRestriction;
			}
			synchronized(pot.accessToPot) {
				if (needRefill()) {
					if (pot.accessToPot.tryLock()) {
						pot.refillRequest = true;
						System.out.printf("\n Savage %d: Request refill", id);
						pot.accessToPot.unlock();

						while (pot.refillRequest) {
							//wait refill	
						}

					}
				}
				if (pot.accessToPot.tryLock()) {
					eat();
					pot.accessToPot.unlock();
				}

			}
		}
	}

	private boolean needRefill() {
		if (pot.size < 1 && pot.refillRequest == false) {
			return true;
		}
		return false;
	}

	private void eat() {
		System.out.printf("\n Savage %d: Eat", id);
		pot.size--;
		System.out.printf("\n Pot capacity: %d ", pot.size);
		limit--;

		if (pot.fairnessLimit <= 1 && limit == 0) {
			pot.fairnessLimit--;
			pot.fairnessFlag = true;
		} else {
			pot.fairnessLimit--;
		}
	}
}