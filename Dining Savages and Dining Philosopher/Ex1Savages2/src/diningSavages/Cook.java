package diningSavages;

public class Cook implements Runnable {
	int id;
	Pot pot;
	int numberSavages;
	int potSize;

	public Cook(int id, Pot pot, int numberSavages) {
		this.id = id;
		this.pot = pot;
		this.numberSavages = numberSavages;
		this.potSize = pot.size;
	}

	@Override
	public void run() {
		System.out.printf("\n -Cook begin his work", id);
		while (true) {
			if (pot.refillRequest) {
				if (pot.accessToPot.tryLock()) {
					refill();
					pot.accessToPot.unlock();
				}
			}
		}
	}

	private void refill() {
		System.out.printf("\n -Cook refill the pot", id);
		pot.size = potSize;
		pot.refillRequest = false;
	}
}