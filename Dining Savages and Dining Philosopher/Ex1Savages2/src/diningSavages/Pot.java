package diningSavages;

import java.util.concurrent.locks.ReentrantLock;

public class Pot {
	int size;
	int numberMealsRestriction;
	ReentrantLock accessToPot;
	boolean refillRequest;
	int fairnessLimit;
	boolean fairnessFlag;
	
	public Pot(int size, ReentrantLock accessToPot, boolean refillRequest, int numberMealsRestriction, int fairnessLimit, boolean fairnessFlag) {
		this.size = size;
		this.numberMealsRestriction = numberMealsRestriction;
		this.accessToPot = accessToPot;
		this.refillRequest = refillRequest;
		this.fairnessLimit = fairnessLimit;
		this.fairnessFlag = fairnessFlag;
	}
}
