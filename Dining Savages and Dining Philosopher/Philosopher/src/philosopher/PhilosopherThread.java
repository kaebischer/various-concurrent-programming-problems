package philosopher;


public class PhilosopherThread implements Runnable {
	int id;
	Object leftFork;
	Object rightFork;
	Boolean isLeftHanded;
	int dinnerQuantity;
	boolean infinity;

	public PhilosopherThread(int id, Object leftFork, Object rightFork, Boolean isLeftHanded, int dinnerQuantity, boolean infinity) {
		this.id = id;
		this.leftFork = leftFork;
		this.rightFork = rightFork;
		this.isLeftHanded = isLeftHanded;
		this.dinnerQuantity = dinnerQuantity;
		this.infinity = infinity;
	}

	@Override
	public void run() {
		while (dinnerQuantity >= 0 || infinity) {

			System.out.printf("\n Philosopher %d: ____Sleeping____", id);

			if(isLeftHanded) {
				synchronized (leftFork) {
					System.out.printf("\n Philosopher %d: |____UpLeftFork____", id);
					synchronized (rightFork) {
						// eating 
						System.out.printf("\n Philosopher %d: |____UpRightFork____|", id);
						System.out.printf("\n Philosopher %d: |____Eating____|", id);
						dinnerQuantity--;
						System.out.printf("\n Philosopher %d: |____DownRightFork____", id);
					}

					// Back to sleep
					System.out.printf("\n Philosopher %d: ____DownLeftFork____", id);
				}
			} else {
				synchronized (rightFork) {
					System.out.printf("\n Philosopher %d: ____UpRightFork____|", id);
					synchronized (leftFork) {
						// eating 
						System.out.printf("\n Philosopher %d: |____UpLeftFork____|", id);
						System.out.printf("\n Philosopher %d: |____Eating____|", id);
						dinnerQuantity--;
						System.out.printf("\n Philosopher %d: ____DownLeftFork____|", id);
					}

					// Back to sleep
					System.out.printf("\n Philosopher %d: ____DownRightFork____", id);
				}
			}
		}
		System.out.printf("\n Philosopher %d: Finished his dinner -%d-", id, dinnerQuantity);
	}
}