package philosopher;

public class Main {

	public static void main(String[] args) {

		int n = (args.length >= 1 ? Integer.parseInt(args[0]) : 4);
		int dinnerQuantity = (args.length >= 2 ? Integer.parseInt(args[1]) : 1);
		Boolean infinity = (args.length >= 3 ? Boolean.parseBoolean(args[2]) : false);
		Boolean isLeftHanded = true;

		// Create forks
		Object[] forks = new Object[n];
		for (int i = 0; i < n; i++) {
			forks[i] = new Object();
		}

		// Create threads
		Thread[] threads = new Thread[n];

		for (int i = 0; i < n; i++) {
			Object leftFork = forks[i];
			Object rightFork = forks[(i + 1) % forks.length];
			if(i == n-1)
				isLeftHanded = false;
			threads[i] = new Thread(new PhilosopherThread(i, leftFork, rightFork, isLeftHanded, dinnerQuantity, infinity));
		}


		System.out.printf("\nStart with %d philosopher\n", n);

		long time = System.currentTimeMillis();
		// Start threads
		for (int i = 0; i < n; i++) {
			threads[i].start();
		}
		// Wait for threads completion
		for (int i = 0; i < n; i++) {
			try {
				threads[i].join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		time = System.currentTimeMillis() - time;
		System.out.printf("\n\n%d ms", time);
	}	
}