package diningSavages;

import java.util.concurrent.locks.ReentrantLock;

public class Pot {
	int size;
	int numberMealsRestriction;
	ReentrantLock accessToPot;
	boolean refillRequest;
	
	public Pot(int size, ReentrantLock accessToPot, boolean refillRequest, int numberMealsRestriction) {
		this.size = size;
		this.numberMealsRestriction = numberMealsRestriction;
		this.accessToPot = accessToPot;
		this.refillRequest = refillRequest;
	}
}
