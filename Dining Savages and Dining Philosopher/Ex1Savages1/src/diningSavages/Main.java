package diningSavages;

import java.util.concurrent.locks.ReentrantLock;

public class Main {
	volatile static Pot pot;
	volatile static ReentrantLock accessToPot;
	volatile static boolean refillRequest;

	public static void main(String[] args) {

		int t = (args.length >= 1 ? Integer.parseInt(args[0]) : 8);
		int numberMealsRestriction = (args.length >= 2 ? Integer.parseInt(args[1]) : 1);
		int potSize = (args.length >= 3 ? Integer.parseInt(args[2]) : 3);

		int numberSavages = t-1;

		accessToPot = new ReentrantLock();
		refillRequest = false;
		pot = new Pot(potSize, accessToPot, refillRequest, numberMealsRestriction);

		// Create threads
		Thread[] threads = new Thread[t];

		for (int i = 0; i < t; i++) {
			if (i == 0) {
				threads[i] = new Thread(new Cook(i, pot, numberSavages));
			} else {
				threads[i] = new Thread(new Savage(i, pot));
			}
		}

		System.out.printf("\nStart with %d savages", numberSavages);
		System.out.printf("\nInitial pot capacity is %d", potSize);
		System.out.printf("\nRestriction of meals is %d\n", numberMealsRestriction);

		long time = System.currentTimeMillis();

		// Start threads
		for (int i = 0; i < t; i++) {
			threads[i].start();
		}
		// Wait for threads completion
		for (int i = 0; i < t; i++) {
			try {
				threads[i].join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		time = System.currentTimeMillis() - time;
		System.out.printf("\n\n%d ms", time);
	}	
}