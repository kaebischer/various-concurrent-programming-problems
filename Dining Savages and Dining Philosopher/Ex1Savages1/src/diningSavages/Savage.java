package diningSavages;

public class Savage implements Runnable {
	int id;
	Pot pot;
	int limit;


	public Savage(int id, Pot pot) {
		this.id = id;
		this.pot = pot;
		this.limit = pot.numberMealsRestriction;
	}

	@Override
	public void run() {
		while (limit > 0) {
			synchronized(pot.accessToPot) {
				if (needRefill()) {
					if (pot.accessToPot.tryLock()) {
						pot.refillRequest = true;
						System.out.printf("\n Savage %d: Request refill", id);
						pot.accessToPot.unlock();

						while (pot.refillRequest) {
							//wait for refill	
						}

					}
				}
				if (pot.accessToPot.tryLock()) {
					eat();
					pot.accessToPot.unlock();
				}

			}
		}
		System.out.printf("\n Savage %d: Finished his dinner", id);
	}

	private boolean needRefill() {
		if (pot.size < 1 && pot.refillRequest == false) {
			return true;
		}
		return false;
	}

	private void eat() {
		System.out.printf("\n Savage %d: Eat", id);
		pot.size--;
		System.out.printf("\n Pot capacity: %d ", pot.size);
		limit--;
	}
}