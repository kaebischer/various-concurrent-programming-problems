package diningSavages;

public class Cook implements Runnable {
	int id;
	Pot pot;
	int numberSavages;
	int nbRequest;
	int potSize;

	public Cook(int id, Pot pot, int numberSavages) {
		this.id = id;
		this.pot = pot;
		this.numberSavages = numberSavages;
		this.potSize = pot.size;
	}

	@Override
	public void run() {
		nbRequest = (int) Math.ceil((double)numberSavages / pot.size) - 1;
		System.out.printf("\n -Cook begin his work", id);

		while (nbRequest > 0) {
			if (pot.refillRequest) {
				if (pot.accessToPot.tryLock()) {
					refill();
					pot.accessToPot.unlock();
				}
			}
		}
		System.out.printf("\n -Cook end his work", id);
	}

	private void refill() {
		System.out.printf("\n -Cook refill the pot", id);
		pot.size = potSize;
		nbRequest--;
		pot.refillRequest = false;
	}
}